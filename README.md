## Screenshots ##
=================

![Screenshot_20151202-192847.png](https://bitbucket.org/repo/XEpokR/images/2920494106-Screenshot_20151202-192847.png)
![Screenshot_20151202-192854.png](https://bitbucket.org/repo/XEpokR/images/3225664971-Screenshot_20151202-192854.png)
![Screenshot_20151202-192923.png](https://bitbucket.org/repo/XEpokR/images/1221618497-Screenshot_20151202-192923.png)
![Screenshot_20151202-192934.png](https://bitbucket.org/repo/XEpokR/images/1291391735-Screenshot_20151202-192934.png)
![Screenshot_20160403-183731.png](https://bitbucket.org/repo/XEpokR/images/3354826637-Screenshot_20160403-183731.png)
![Screenshot_20160403-183750.png](https://bitbucket.org/repo/XEpokR/images/3005103704-Screenshot_20160403-183750.png)
![Screenshot_20160403-185932.png](https://bitbucket.org/repo/XEpokR/images/3609943330-Screenshot_20160403-185932.png)


## Sample ##
=============

You can download the sample app [here](https://play.google.com/store/apps/details?id=org.bitbucket.stefanodp91.android.wizardpager)

## Description ##
=================

Wizard Pager is a library that provides an example implementation of a Wizard UI on Android, it's based of Roman Nurik's wizard pager (https://github.com/romannurik/android-wizardpager) and Tech Freak's wizard pager (https://github.com/str4d/WizardPager)

I've updated Tech Freak's code. Now it is even easier to use this library.

## Download ##
============

You can get the library via Gradle:

```groovy
dependencies {
   compile 'org.bitbucket.stefanodp91:wizard-pager:0.1'
}
```


## Usage ##
============

There is a sample implementation, so you can see how to add this library to your projects.

### Setting Manifest.xml ###

First of all you have add these permissions to your manifest:

![Screenshot_1.png](https://bitbucket.org/repo/XEpokR/images/3553732840-Screenshot_1.png)

**Don't forget to add the "replace" statement.**

### Define your own model ###

Now you have to define your own custom model extending from **AbstractWizardModel** class.

You can follow the sample app to learn how.

![Screenshot_2.png](https://bitbucket.org/repo/XEpokR/images/2298140590-Screenshot_2.png)

### Define your own activity ###

Than you have to extend your Activity from **WizardActivity** ( which extends AppCompactActivity)

Call the method **initWizard(new MyOwnModel(myContext));** to set the model.

![Screenshot_3.png](https://bitbucket.org/repo/XEpokR/images/2200725869-Screenshot_3.png)

Override the review action button to set your own action.

The method  **getReviewItems();** allows you to retrieve the insered data.

![Screenshot_4.png](https://bitbucket.org/repo/XEpokR/images/3914883723-Screenshot_4.png)


## Known bugs ##
============

1. You have to add dependecy to [Glide](https://github.com/bumptech/glide) to load and cache images, otherwise the app will crash.
2. The wizard supports the use of a single map for a time.



## Developed By ##
============

Stefano de Pascalis - <stefanodp91dev@gmail.com>


## License ##
=======

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.