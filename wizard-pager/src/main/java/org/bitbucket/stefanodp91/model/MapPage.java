package org.bitbucket.stefanodp91.model;

import android.support.v4.app.Fragment;

import org.bitbucket.stefanodp91.ui.map.MapFragment;


/**
 * Created by stefano on 25/11/2015.
 */
public class MapPage extends TextPage {
    public MapPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return MapFragment.create(getKey());
    }
}
