/*
 * Copyright 2012 Roman Nurik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.stefanodp91.ui;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.bitbucket.stefanodp91.R;
import org.bitbucket.stefanodp91.model.AbstractWizardModel;
import org.bitbucket.stefanodp91.model.ModelCallbacks;
import org.bitbucket.stefanodp91.model.Page;
import org.bitbucket.stefanodp91.model.ReviewItem;
import org.bitbucket.stefanodp91.utils.Constants;
import org.bitbucket.stefanodp91.utils.ImageUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class ReviewFragment extends ListFragment implements ModelCallbacks {
    private static final String TAG = ReviewFragment.class.getName();

    private Callbacks mCallbacks;
    private AbstractWizardModel mWizardModel;
    private List<ReviewItem> mCurrentReviewItems;

    private ReviewAdapter mReviewAdapter;
    private ImageUtils imageUtils;
    private ImageView imageView;


    public ReviewFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mReviewAdapter = new ReviewAdapter();
        imageUtils = new ImageUtils(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_page, container, false);

        TextView titleView = (TextView) rootView.findViewById(android.R.id.title);
        titleView.setText(R.string.review);
        titleView.setTextColor(getResources().getColor(R.color.color_review));

        ListView listView = (ListView) rootView.findViewById(android.R.id.list);
        setListAdapter(mReviewAdapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof Callbacks)) {
            throw new ClassCastException("Activity must implement fragment's callbacks");
        }

        imageUtils = new ImageUtils(getContext());
        mCallbacks = (Callbacks) activity;

        mWizardModel = mCallbacks.onGetModel();
        mWizardModel.registerListener(this);
        onPageTreeChanged();
    }

    @Override
    public void onPageTreeChanged() {
        onPageDataChanged(null);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;

        imageUtils = null;
        mWizardModel.unregisterListener(this);
    }

    @Override
    public void onPageDataChanged(Page changedPage) {
        ArrayList<ReviewItem> reviewItems = new ArrayList<ReviewItem>();
        for (Page page : mWizardModel.getCurrentPageSequence()) {
            page.getReviewItems(reviewItems);
        }
        Collections.sort(reviewItems, new Comparator<ReviewItem>() {
            @Override
            public int compare(ReviewItem a, ReviewItem b) {
                return a.getWeight() > b.getWeight() ? +1 : a.getWeight() < b.getWeight() ? -1 : 0;
            }
        });
        mCurrentReviewItems = reviewItems;

        if (mReviewAdapter != null) {
            mReviewAdapter.notifyDataSetInvalidated();
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        mCallbacks.onEditScreenAfterReview(mCurrentReviewItems.get(position).getPageKey());
    }

    public interface Callbacks {
        AbstractWizardModel onGetModel();

        void onEditScreenAfterReview(String pageKey);
    }

    private class ReviewAdapter extends BaseAdapter {
        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public Object getItem(int position) {
            return mCurrentReviewItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return mCurrentReviewItems.get(position).hashCode();
        }

        @Override
        public View getView(int position, View view, ViewGroup container) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View rootView = inflater.inflate(R.layout.list_item_review, container, false);

            ReviewItem reviewItem = mCurrentReviewItems.get(position);
            String value = reviewItem.getDisplayValue();
            if (org.bitbucket.stefanodp91.utils.TextUtils.isValid(value)) {
                try {
                    JSONObject jsonObject = new JSONObject(value);
                    String jsonType = jsonObject.getString(Constants.JSON_TYPE);
                    if (jsonType.compareTo(Constants.JSON_TYPE_MAP) == 0) {
                        String nearCity = jsonObject.getString(Constants.JSON_NEAR_CITY);
                        value = nearCity;

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.JSON_BUNDLE_MAP, jsonObject.toString());
                        reviewItem.setExtra(bundle);
                    }

                    if (jsonType.compareTo(Constants.JSON_TYPE_DATE) == 0) {
                        String dateFrom = jsonObject.getString(Constants.JSON_DATE_FROM);
                        String dateTo = jsonObject.getString(Constants.JSON_DATE_TO);
                        value = getString(R.string.label_date_from) + ": " + dateFrom + "\n" +
                                getString(R.string.label_date_to) + ": " + dateTo;

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.JSON_BUNDLE_DATE, jsonObject.toString());
                        reviewItem.setExtra(bundle);
                    }

                    if (jsonType.compareTo(Constants.JSON_TYPE_TITLE) == 0) {
                        String title = jsonObject.getString(Constants.JSON_TITLE);

                        value = title;

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.JSON_BUNDLE_TITLE, jsonObject.toString());
                        reviewItem.setExtra(bundle);
                    }

                    if (jsonType.compareTo(Constants.JSON_TYPE_TEXT) == 0) {
                        String text = jsonObject.getString(Constants.JSON_TEXT);

                        value = text;

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.JSON_BUNDLE_TEXT, jsonObject.toString());
                        reviewItem.setExtra(bundle);
                    }

                    if (jsonType.compareTo(Constants.JSON_TYPE_NOTES) == 0) {
                        String notes = jsonObject.getString(Constants.JSON_TEXT);

                        value = notes;

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.JSON_BUNDLE_NOTES, jsonObject.toString());
                        reviewItem.setExtra(bundle);
                    }

                    if (jsonType.compareTo(Constants.JSON_TYPE_PASSWORD) == 0) {
                        String notes = jsonObject.getString(Constants.JSON_PASSWORD);

                        value = notes;

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.JSON_BUNDLE_PASSWORD, jsonObject.toString());
                        reviewItem.setExtra(bundle);
                    }

                    if (jsonType.compareTo(Constants.JSON_TYPE_ITEM_PICKER) == 0) {
                        String item = jsonObject.getString(Constants.JSON_ITEM_PICKER);

                        value = item;

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.JSON_BUNDLE_ITEM_PICKER, jsonObject.toString());
                        reviewItem.setExtra(bundle);
                    }

                    if (jsonType.compareTo(Constants.JSON_TYPE_DAYS_PICKER) == 0) {
                        String item = jsonObject.getString(Constants.JSON_DAYS_PICKER);

                        value = item;

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.JSON_BUNDLE_DAYS_PICKER, jsonObject.toString());
                        reviewItem.setExtra(bundle);
                    }

                    if (jsonType.compareTo(Constants.JSON_TYPE_IMAGE) == 0) {
                        String imageUri = jsonObject.getString(Constants.JSON_IMAGE_URI);

                        value = imageUri;

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.JSON_BUNDLE_IMAGE, jsonObject.toString());
                        reviewItem.setExtra(bundle);

                        // show the image
                        if (imageUtils.isPhotoFile(value)) {
                            ((TextView) rootView.findViewById(android.R.id.text2)).setVisibility(View.GONE);
                            imageView = (ImageView) rootView.findViewById(R.id.image1);
                            imageUtils.display(imageView, Uri.parse(value));
                        }
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "setReviewButtonAction() ==> failed with JSONException. " + e.getMessage());
                }
            }
            // show a no data message
            if (TextUtils.isEmpty(value)) {
                value = getString(R.string.no_item_selected);
            }

            // show datas if they exists
            ((TextView) rootView.findViewById(android.R.id.text1)).setText(reviewItem.getTitle());
            ((TextView) rootView.findViewById(android.R.id.text2)).setText(value);
            return rootView;
        }

        @Override
        public int getCount() {
            return mCurrentReviewItems.size();
        }
    }

    public AbstractWizardModel getWizardModel() {
        return mWizardModel;
    }

    public void setWizardModel(AbstractWizardModel mWizardModel) {
        this.mWizardModel = mWizardModel;
    }

    public Callbacks getCallbacks() {
        return mCallbacks;
    }

    public void setCallbacks(Callbacks mCallbacks) {
        this.mCallbacks = mCallbacks;
    }
}
