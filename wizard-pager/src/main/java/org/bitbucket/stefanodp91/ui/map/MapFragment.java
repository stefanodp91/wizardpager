package org.bitbucket.stefanodp91.ui.map;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.bitbucket.stefanodp91.R;
import org.bitbucket.stefanodp91.model.Page;
import org.bitbucket.stefanodp91.ui.PageFragmentCallbacks;
import org.bitbucket.stefanodp91.utils.Constants;
import org.bitbucket.stefanodp91.utils.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class MapFragment extends Fragment implements PlaceSelectionListener, OnMapReadyCallback {
    private static final String TAG = MapFragment.class.getName();

    protected static final String ARG_KEY = "key";

    private PageFragmentCallbacks mCallbacks;
    private String mKey;
    private Page mPage;

    /* map */
    private static final int MAX_RESULTS_NUMBER = 1;
    private static final String LAT = "LAT";
    private static final String LNG = "LNG";
    private static final int MAP_ANIMATION_DURATION = 1000;//millis
    private static final int MAP_ZOOM = 10;
    private PlaceAutocompleteFragment autocompleteFragment;
    private MyMapFragment mapFragment;
    private MapHelper mapHelper;
    /* end map */

    private static View rootView;

    public static MapFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        MapFragment f = new MapFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = mCallbacks.onGetPage(mKey);

        mapHelper = new MapHelper(getContext());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }

        try {
            rootView = inflater.inflate(R.layout.fragment_page_map, container, false);
        } catch (InflateException e) {
        /* view is already there, just return view as it is */
            Log.e(TAG, "onCreateView() ==> failed with InflateException. " + e.getMessage());
        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((TextView) view.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mapFragment = (MyMapFragment) getActivity().getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Retrieve the PlaceAutocompleteFragment.
        autocompleteFragment = (PlaceAutocompleteFragment)
                getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        /*
         crea un filtro per il recupero delle sole città .
         senza questo filtro, vengono recuperati anche luoghi nelle vicinanze, professionisti, ecc..
         */
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build();
        autocompleteFragment.setFilter(typeFilter);

        // Register a listener to receive callbacks when a place has been selected or an error has
        // occurred.
        autocompleteFragment.setOnPlaceSelectedListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException(
                    "Activity must implement PageFragmentCallbacks");
        }

        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    /*
     * Quando la mappa viene renderizzata, se:
     * 1) sono state passate latitudine e longituine nel fragment (presi dell'autocomplete, allora
     * visualizza il luogo sulla mappa;
     * 2) se non ci sono latitudine e longitudine, prende quelle correnti e visualizza la posizione
     * corrente sulla mappa
     *
     * @param map
     */
    @Override
    public void onMapReady(GoogleMap map) {
        if (map != null) {

            map.clear(); // cancella i marker precedemente creati presenti nella mappa

            CameraPosition position;
            double lat, lng;
            LatLng placePosition;
            Bundle arguments = mapFragment.getDatas();
            String nearCity = "";

            if (arguments != null) {
                // autocomplete place latlng
                lat = arguments.getDouble(LAT);
                lng = arguments.getDouble(LNG);

                try {
                    nearCity = mapHelper.getNearCity(lat, lng);
                } catch (IOException e) {
                    Log.e(TAG, "updateButtonsStatus(...) ==> failed with IOException." + e.getMessage());
                }

                placePosition = new LatLng(lat, lng);

                position = mapHelper.setCameraPosition(placePosition, MAP_ZOOM);

                map.addMarker(new MarkerOptions().position(placePosition).title(nearCity));
                map.animateCamera(CameraUpdateFactory.newCameraPosition(position), MAP_ANIMATION_DURATION, null);
                updateLocation(placePosition, nearCity);

            } else {
                // current place latlng
                Log.i(TAG, "onMapReady(...) ==> arguments is null");

                // Enabling MyLocation Layer of Google Map
                map.setMyLocationEnabled(true);

                // Getting LocationManager object from System Service LOCATION_SERVICE
                LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

                // Creating a criteria object to retrieve provider
                Criteria criteria = new Criteria();

                // Getting the name of the best provider
                String provider = locationManager.getBestProvider(criteria, true);

                // Getting Current Location
                Location location = locationManager.getLastKnownLocation(provider);

                if (location != null) {
                    // Getting latitude of the current location
                    lat = location.getLatitude();

                    // Getting longitude of the current location
                    lng = location.getLongitude();

                    // Creating a LatLng object for the current location
                    placePosition = new LatLng(lat, lng);

                    position = mapHelper.setCameraPosition(placePosition, MAP_ZOOM);

                    try {
                        nearCity = mapHelper.getNearCity(lat, lng);
                    } catch (IOException e) {
                        Log.e(TAG, "updateButtonsStatus(...) ==> failed with IOException." + e.getMessage());
                    }

                    map.addMarker(new MarkerOptions().position(placePosition).title(nearCity));
                    map.animateCamera(CameraUpdateFactory.newCameraPosition(position), MAP_ANIMATION_DURATION, null);

                    updateSuggestion(nearCity); // mostra la posizione corrente nella suggestionview
                    updateLocation(placePosition, nearCity);
                }
            }
        } else {
            Log.e(TAG, "onMapReady(...) ==> map is null");
        }
    }

    /**
     * Callback invoked when a place has been selected from the PlaceAutocompleteFragment.
     */
    @Override
    public void onPlaceSelected(Place place) {
        if (place.isDataValid()) {
            Log.i(TAG, "onPlaceSelected(...) ==> placeName ==  " + place.getName());

            try {
                showMap(place);
            } catch (GooglePlayServicesRepairableException e) {
                Log.e(TAG, "onPlaceSelected(...) ==> failed with GooglePlayServicesRepairableException. " + e.getMessage());
            } catch (GooglePlayServicesNotAvailableException e) {
                Log.e(TAG, "onPlaceSelected(...) ==> failed with GooglePlayServicesNotAvailableException. " + e.getMessage());
            }
        } else {
            Log.e(TAG, "onPlaceSelected(...) ==> place is null");
        }
    }

    /**
     * Callback invoked when PlaceAutocompleteFragment encounters an error.
     */
    @Override
    public void onError(Status status) {
        Log.e(TAG, "onError(...) ==> status == " + status.toString());

        Snackbar.make(getActivity().findViewById(android.R.id.content), "Place selection failed: " + status.getStatusMessage(),
                Snackbar.LENGTH_SHORT).show();
    }

    private void showMap(Place place) throws GooglePlayServicesNotAvailableException,
            GooglePlayServicesRepairableException {

        LatLng latLng = place.getLatLng();
        double lat = latLng.latitude;
        double lng = latLng.longitude;

        Bundle arguments = new Bundle();
        arguments.putDouble(LAT, lat);
        arguments.putDouble(LNG, lng);

        mapFragment.setDatas(arguments);
        mapFragment.getMapAsync(this);
    }

    private void updateSuggestion(String suggestion) {
        autocompleteFragment.setText(suggestion);
    }

    private void updateLocation(LatLng latLng, String nearCity) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Constants.JSON_TYPE, Constants.JSON_TYPE_MAP);
        } catch (JSONException e) {
            Log.e(TAG, "updateLocation() ==> JSON_TYPE failed with JSONException. " + e.getMessage());
        }

        try {
            jsonObject.put(Constants.JSON_PAGE_TITLE, mPage.getTitle());
        } catch (JSONException e) {
            Log.e(TAG, "updateLocation() ==> JSON_PAGE_TITLE failed with JSONException. " + e.getMessage());
        }

        try {
            jsonObject.put(Constants.JSON_NEAR_CITY, nearCity);
        } catch (JSONException e) {
            Log.e(TAG, "updateLocation() ==> NEAR_CITY failed with JSONException. " + e.getMessage());
        }

        try {
            jsonObject.put(Constants.JSON_LAT, latLng.latitude);
        } catch (JSONException e) {
            Log.e(TAG, "updateLocation() ==> JSON_LAT failed with JSONException. " + e.getMessage());
        }

        try {
            jsonObject.put(Constants.JSON_LNG, latLng.longitude);
        } catch (JSONException e) {
            Log.e(TAG, "updateLocation() ==> JSON_LNG failed with JSONException. " + e.getMessage());
        }

        mPage.getData().putString(Page.SIMPLE_DATA_KEY, jsonObject.toString());
        mPage.notifyDataChanged();
    }

    private static class MapHelper {
        private Context context;

        public MapHelper(Context context) {
            this.context = context;
        }

        // restituisce il nome della città più vicina alle coordinate inserite
        public String getNearCity(double lat, double lng) throws IOException {
            String nearCity = "";
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(lat, lng, MAX_RESULTS_NUMBER);
            if (addresses.size() > 0) {
                nearCity = addresses.get(0).getLocality();
                Log.i(TAG, "getNearCity(...) ==> nearCity == " + addresses.get(0).getLocality());
            }
            return nearCity;
        }

        // zoom sulla mappa nella posizione attuale
        public CameraPosition setCameraPosition(LatLng position, int zoom) {
            return CameraPosition.builder()
                    .target(position)
                    .zoom(zoom)
                    .bearing(0)
                    .tilt(45)
                    .build();
        }
    }
}