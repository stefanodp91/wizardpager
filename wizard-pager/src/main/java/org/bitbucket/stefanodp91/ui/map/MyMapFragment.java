package org.bitbucket.stefanodp91.ui.map;

import android.os.Bundle;

import com.google.android.gms.maps.MapFragment;

/**
 * Created by stefano on 15/03/16.
 * Questo fragment estende mapfragment aggiungendo la possibilità di passare parametri anche
 * quando il fragment è in uso.
 */
public class MyMapFragment extends MapFragment {
    private Bundle bundle;

    public void setDatas(Bundle bundle) {
        this.bundle = bundle;
    }

    public Bundle getDatas() {
        return bundle;
    }
}
