/*
 * Copyright 2012 Roman Nurik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.stefanodp91.android.wizardpager;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Toast;

import org.bitbucket.stefanodp91.WizardActivity;
import org.bitbucket.stefanodp91.model.ReviewItem;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


public class MainActivity extends WizardActivity {
    private static final String TAG = MainActivity.class.getName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // just call this method passing the model to start the wizard
        initWizard(new WizardModel(this));
    }

    /**
     * Custom action when review button is pressed.
     */
    @Override
    protected void setReviewButtonAction() {
        Toast.makeText(getApplicationContext(), "Superclass function has been replaced", Toast.LENGTH_LONG).show();


        List<ReviewItem> mList = getReviewItems();

        String toShow = "";

        for (ReviewItem r : mList) {
            toShow += r.getTitle() + " == " + r.getDisplayValue() + "\n";

            String toPrint = r.getDisplayValue();
            Log.i(TAG, "item == " + toPrint);
        }

        final String finalToShow = toShow;
        DialogFragment dg = new DialogFragment() {
            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                return new AlertDialog.Builder(getActivity())
                        .setMessage(finalToShow)
                        .setPositiveButton(R.string.submit_confirm_button, null)
                        .setNegativeButton(android.R.string.cancel, null).create();
            }
        };
        dg.show(getSupportFragmentManager(), "place_order_dialog");
    }
}
