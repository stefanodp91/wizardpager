/*
 * Copyright 2012 Roman Nurik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.stefanodp91.android.wizardpager;

import android.content.Context;

import org.bitbucket.stefanodp91.model.AbstractWizardModel;
import org.bitbucket.stefanodp91.model.BranchPage;
import org.bitbucket.stefanodp91.model.DatePage;
import org.bitbucket.stefanodp91.model.DaysPickerPage;
import org.bitbucket.stefanodp91.model.ImagePage;
import org.bitbucket.stefanodp91.model.MapPage;
import org.bitbucket.stefanodp91.model.MultipleFixedChoicePage;
import org.bitbucket.stefanodp91.model.NotePage;
import org.bitbucket.stefanodp91.model.NumberPage;
import org.bitbucket.stefanodp91.model.PageList;
import org.bitbucket.stefanodp91.model.PasswordPage;
import org.bitbucket.stefanodp91.model.SingleFixedChoicePage;
import org.bitbucket.stefanodp91.model.TitlePage;


public class WizardModel extends AbstractWizardModel {
    public WizardModel(Context context) {
        super(context);
    }

    @Override
    protected PageList onNewRootPageList() {
        return new PageList(new BranchPage(this, "Order type").addBranch(
                "Sandwich",
                new SingleFixedChoicePage(this, "Bread").setChoices("White",
                        "Wheat", "Rye", "Pretzel", "Ciabatta")
                        .setRequired(true),

                new MultipleFixedChoicePage(this, "Meats").setChoices(
                        "Pepperoni", "Turkey", "Ham", "Pastrami", "Roast Beef",
                        "Bologna"),

                new MultipleFixedChoicePage(this, "Veggies").setChoices(
                        "Tomatoes", "Lettuce", "Onions", "Pickles",
                        "Cucumbers", "Peppers"),

                new MultipleFixedChoicePage(this, "Cheeses").setChoices(
                        "Swiss", "American", "Pepperjack", "Muenster",
                        "Provolone", "White American", "Cheddar", "Bleu"),

                new BranchPage(this, "Toasted?")
                        .addBranch(
                                "Yes",
                                new SingleFixedChoicePage(this, "Toast time")
                                        .setChoices("30 seconds", "1 minute",
                                                "2 minutes")).addBranch("No")
                        .setValue("No"))

                .addBranch(
                        "Salad",
                        new SingleFixedChoicePage(this, "Salad type").setChoices(
                                "Greek", "Caesar").setRequired(true),

                        new SingleFixedChoicePage(this, "Dressing").setChoices(
                                "No dressing", "Balsamic", "Oil & vinegar",
                                "Thousand Island", "Italian").setValue("No dressing"),
                        new NumberPage(this, "How Many Salads?").setRequired(true))


                .addBranch("Demo",
                        new TitlePage(this, "Title"),
                        new NotePage(this, "Notes"),
                        new ImagePage(this, "Image"),
//                        new MapPage(this, "Map"),
                        new DatePage(this, "Date"),
                        new DaysPickerPage(this, "Days picker", 50),
                        new PasswordPage(this, "Password"))
        );
    }
}
